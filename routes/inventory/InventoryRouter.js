const express =require('express');
const InventoryController = require('./InventoryController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createInventory',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),InventoryController.createInventory);

router.get("/subtractproductfrominventory",InventoryController.subtractproductfrominventory)
router.get("/removeproductfrominventory/:id",InventoryController.removeproductfrominventory)
router.get("/updateproductininventory",InventoryController.updateproductininventory)
router.get("/markproductasinactiveininventory",InventoryController.markproductasinactiveininventory)
router.get("/markproductasactiveininventory",InventoryController.markproductasactiveininventory)
router.get("/checkifproductisininventory",InventoryController.checkifproductisininventory)

module.exports=router