const GlobalSettings=require("../../models/admin/GlobalSettings");

module.exports={
    updateoraddSettings:async(req,res)=>{
        try{
            console.log("req.body ", req.body);
            const {
                tax,
                commission,
                plusperproduct,
                returnprice
            }=req.body
            const settings=await GlobalSettings.findOne({});
            if(settings){
                await GlobalSettings.findByIdAndUpdate(settings._id,{
                    ...(tax&&{tax:tax}),
                    ...(commission&&{commission:commission}),
                    ...(returnprice&&{returnprice:returnprice}),

                    ...(plusperproduct&&{plusperproduct:plusperproduct})
                }).then((result)=>{
                    return res.send({
                        status:true,
                        data:result
                    })
                })
            }else{
                const newsettings=new GlobalSettings();
                newsettings.tax=tax;
                newsettings.commission=commission;
                newsettings.plusperproduct=plusperproduct;
                newsettings.save()
                            .then((result)=>{
                            return res.send({
                                status:true,
                                data:result
                            })
                        })
            }
        }catch(error){
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
    },
    getglobalsettings:async(req,res)=>{
        try{
            const settings=await GlobalSettings.findOne({})||[];
            return res.send({
                    status:true,
                    data:settings
                })
        }catch(error)
        {
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
    }
}