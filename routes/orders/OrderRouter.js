const express =require('express');
const OrderController = require('./OrderController');
const OrderSubController = require ("./OrderSubController");
const router = express.Router();
const multer=require("multer");
const path=require("path");
const PRODUCT_PATH=process.env.PRODUCT_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(PRODUCT_PATH))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createorder',OrderController.createorders);
router.post('/getorderbyid',OrderController.getorderbyid);


router.get('/getallordersbyuseridCount/:userid',OrderController.getallordersbyuseridCount);
router.get('/getallordersbyuserid/:userid/:page_no',OrderController.getallordersbyuserid);

router.get('/getAllBidOrdersByUserIdCount/:userid',OrderController.getAllBidOrdersByUserIdCount);
router.get('/getAllBidOrdersByUserId/:userid/:page_no',OrderController.getAllBidOrdersByUserId);



router.post('/getallordersbyuseridPostMethod',OrderController.getallordersbyuseridPostMethod);


router.get('/getallprocessingordersbyuseridCount/:userid',OrderController.getallprocessingordersbyuseridCount);
router.get('/getallprocessingordersbyuserid/:userid/:page_no',OrderController.getallprocessingordersbyuserid);
router.get('/getallcompletedordersbyuserid/:userid',OrderController.getallcompletedordersbyuserid);
router.get('/getallcancelledordersbyuserid/:userid/:page_no',OrderController.getallcancelledordersbyuserid);
router.get('/getallcancelledordersbyuseridCount/:userid',OrderController.getallcancelledordersbyuseridCount);



router.get('/getorderdetailbyorderid/:id',OrderController.getorderdetailbyorderid);
router.get('/get_order_detail_by_combo_id/:id',OrderController.get_order_detail_by_combo_id);
router.post('/get_order_detail_by_part_id',OrderController.get_order_detail_by_part_id);
router.post('/cancel_order_by_part_id',OrderController.cancel_order_by_part_id);
router.post('/return_order_by_part_id',OrderController.return_order_by_part_id);

router.post('/getallordersbysellerid',OrderController.getallordersbysellerid);

router.post('/getCancelOrdersBySellerId',OrderController.getCancelOrdersBySellerId);
router.post('/getCompletedOrdersBySellerId',OrderController.getCompletedOrdersBySellerId);
router.post('/getProcessOrdersBySellerId',OrderController.getProcessOrdersBySellerId);


router.post('/getallordersbyselleridnew',OrderController.getallordersbyselleridnew);
router.post('/createrating',uploadTo.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }
]
),OrderController.saveratingandreview);
router.get('/getratingandreviewbyproductid/:product_id',OrderController.getratingandreviewbyproductid)
router.get('/getreviewsbyuserid/:user_id',OrderController.getratingandreviewbyuserid)
router.get('/deletereviewbyreviewid/:review_id',OrderController.deleteratingandreview)
router.post('/getshopratings',OrderController.getshopratings)
router.post("/replytoreview",OrderController.replytoreview)
//update delivery status
router.get('/markOderasCancelledforshipping/:id',OrderController.markOderasCancelledforshipping)
router.get('/markOderasShippedforshipping/:id',OrderController.markOderasShippedforshipping);
router.get('/markOderasCompletedforshipping/:id',OrderController.markOderasCompletedforshipping);

router.get('/markOderasCompleted/:id',OrderController.markOderasCompleted)
router.get('/markOderasCancelled/:id',OrderController.markOderasCancelled)
router.get('/markOderasReturned/:id',OrderController.markOderasReturned)
router.get('/markOderasRefunded/:id',OrderController.markOderasRefunded)
router.post('/markOderasReadytopickup',OrderController.markOderasReadytopickup)

//update delivery status
router.get('/markDeliveryasPending/:id',OrderController.markDeliveryasPending)
router.get('/markDeliveryasAssingedPickup/:id',OrderController.markDeliveryasAssingedPickup)
router.get('/markDeliveryasAssingedDelivery/:id',OrderController.markDeliveryasAssingedDelivery)
router.get('/markDeliveryasDelivered/:id',OrderController.markDeliveryasDelivered)
router.post('/updateorderstatus',OrderController.updateorderstatus)
//delivery apis
router.post('/updatetrackingdetails',OrderController.updatetrackingdetails)
router.get('/getTrackingDetails/:id/:order_id',OrderController.getTrackingDetails)
router.post('/getaddressbypartid',OrderController.getaddressbypartid)
router.get('/getorderdeliveryaddress/:id',OrderController.getorderdeliveryaddress)
router.get('/getallreadytopickuporderparts',OrderController.getallreadytopickuporderparts)
router.get('/getallorderdeliveryaddress',OrderController.getallorderdeliveryaddress)
router.post('/markOderaspickupd',OrderController.markOderaspickupup)


router.get('/requestPayment/:id',OrderSubController.requestPayment);
router.get('/getSuccessDGPayment',OrderSubController.getSuccessDGPayment);
router.get('/getCancelDGPayment',OrderSubController.getCancelDGPayment);
router.get('/getFailedDGPayment',OrderSubController.getFailedDGPayment);

router.post('/ipn',OrderSubController.ipn);
router.get('/requestPaymentAuction/:id',OrderSubController.requestPaymentAuction);
router.get('/getSuccessDGPaymentAuction',OrderSubController.getSuccessDGPaymentAuction);
router.get('/getCancelDGPaymentAuction',OrderSubController.getCancelDGPaymentAuction);
router.get('/getFailedDGPaymentAuction',OrderSubController.getFailedDGPaymentAuction);
router.get('/getPaymentForSub/:id/:amount',OrderSubController.getPaymentForSub);
 
router.get('/getSuccessPaymentSellerSubscription',OrderSubController.getSuccessPaymentSellerSubscription);
router.get('/getCancelPaymentSellerSubscription',OrderSubController.getCancelPaymentSellerSubscription);
router.get('/getFailedSellerSubscription',OrderSubController.getFailedSellerSubscription);



//http://localhost:9001/api/order/getSuccessPaymentSellerSubscription?payment_id=65a5285b4533813037045ff6

module.exports=router