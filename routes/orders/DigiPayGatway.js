"use strict";
const request = require("request");
module.exports =   class DigiPayGatway{
     _amount ;
     _currency = 'XOF' ;
     _app_key ;
     _secrete_key  ;
     _secrete_key_test   ;
     _public_key   ;
     _public_key_test  ;
     _ref_commande ;
     _commande_name ;
     _mode ='test'  ;//prod|test;
     _success_url ='https://client.bgpay.digital/boutiques/success' ;
     _ipn_url  ='https://client.bgpay.digital/boutiques/ipd' ;
     _cancel_url   ='https://client.bgpay.digital/boutiques/cancel' ;
     _failed_url  ='https://client.bgpay.digital/boutiques/failed' ;
     _data_transactions = {} ;
     _client_name = '' ;
     _client_phone = '' ;
     _client_email = '' ;
    static _MODE_TEST = 'test';
    static _MODE_PROD = 'prod';


    get client_name() {
        return this._client_name;
    }

    set client_name(value) {
        this._client_name = value;
    }

    get client_phone() {
        return this._client_phone;
    }

    set client_phone(value) {
        this._client_phone = value;
    }

    get client_email() {
        return this._client_email;
    }

    set client_email(value) {
        this._client_email = value;
    }

    get amount() {
        return this._amount;
    }

    set amount(value) {
        this._amount = value;
    }

    get currency() {
        return this._currency;
    }

    set currency(value) {
        this._currency = value;
    }

    get app_key() {
        return this._app_key;
    }

    set app_key(value) {
        this._app_key = value;
    }

    get secrete_key() {
        return this._secrete_key;
    }

    set secrete_key(value) {
        this._secrete_key = value;
    }

    get secrete_key_test() {
        return this._secrete_key_test;
    }

    set secrete_key_test(value) {
        this._secrete_key_test = value;
    }

    get public_key() {
        return this._public_key;
    }

    set public_key(value) {
        this._public_key = value;
    }

    get public_key_test() {
        return this._public_key_test;
    }

    set public_key_test(value) {
        this._public_key_test = value;
    }

    get ref_commande() {
        return this._ref_commande;
    }

    set ref_commande(value) {
        this._ref_commande = value;
    }

    get commande_name() {
        return this._commande_name;
    }

    set commande_name(value) {
        this._commande_name = value;
    }

    get mode() {
        return this._mode;
    }

    set mode(value) {
        this._mode = value;
    }

    get success_url() {
        return this._success_url;
    }

    set success_url(value) {
        this._success_url = value;
    }

    get ipn_url() {
        return this._ipn_url;
    }

    set ipn_url(value) {
        this._ipn_url = value;
    }

    get cancel_url() {
        return this._cancel_url;
    }

    set cancel_url(value) {
        this._cancel_url = value;
    }

    get failed_url() {
        return this._failed_url;
    }

    set failed_url(value) {
        this._failed_url = value;
    }

    get data_transactions() {
        return this._data_transactions;
    }

    set data_transactions(value) {
        this._data_transactions = value;
    }

    static get MODE_TEST() {
        return this._MODE_TEST;
    }

    static set MODE_TEST(value) {
        this._MODE_TEST = value;
    }

    static get MODE_PROD() {
        return this._MODE_PROD;
    }

    static set MODE_PROD(value) {
        this._MODE_PROD = value;
    }
  async  start(){
    console.log("hereeeeeeeeeee line no. 186");
        let data = {
            app_key:this._app_key,
            secrete_key:this._mode ==='prod' ? this._secrete_key : this._secrete_key_test,
            public_key:this._mode ==='prod' ? this._public_key : this._public_key_test,
            amount:this._amount,
            commande_name:this._commande_name,
            ref_commande:this._ref_commande,
            currency:this._currency,
            data_transactions: JSON.stringify(this._data_transactions),
            mode:this._mode,
            ipn_url:this._ipn_url,
            success_url:this._success_url,
            cancel_url:this._cancel_url,
            failed_url:this._failed_url,
            client_name:this._client_name,
            client_phone:this._client_phone,
            client_email:this._client_email,
        }
      //  console.log(data)
        let options = {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/json'
            },
            "body": JSON.stringify(data),
            "mode": "cors",
        };
        console.log("options ", options);
        let res = await request('https://proxy.bgpay.digital/api/payment/request',options);
        // console.log("res");
        // console.log(res);
        //return JSON.parse(res );
        return res;
    }
};