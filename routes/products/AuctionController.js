const { Product } = require("../../models/products/Product");
const UserWebHistoryModel = require("../../models/products/UserWebHistoryModel");
//const { Product } = require("../../models/products/Product");
const OrderModel = require("../../models/orders/order");
const ProductstempimagesModel = require("../../models/products/ProductstempimagesModel");
const MatCatagory=require("../../models/products/MatCatagory");
const Variations =require("../../models/products/Variations");
const VariationsPrice =require("../../models/products/VariationPrice");
const pageViewsModel=require("../../models/products/pageViews");
const mongoose=require("mongoose");
const catagorymodel = require("../../models/products/Catagory");
const ProductfavoriteModel = require("../../models/products/ProductfavoriteModel");
const CoupensModel=require("../../models/seller/Coupens");
const Ratingnreviews=require("../../models/actions/Ratingnreviews");
const SettingModel = require("../../models/admin/SettingModel");
const Subscriptions=require("../../models/subscriptions/Subscription");
const ShippingModel = require("../../models/shipping/Shipping");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const SellerMAuodel=require("../../models/seller/Seller");
const AuctionbidsModel = require("../../models/products/AuctionbidsModel");
const AuctioninitiateModel = require("../../models/products/AuctioninitiateModel")
const  ProductModel  = require("../../models/products/Product");

const  customConstant  = require("../../helpers/customConstant");
const nodemailer = require("nodemailer");
//
const fs = require("fs");
const { forEach } = require("async");
const transport = nodemailer.createTransport({
  name: "Rawgadou",
  host: process.env.MAILER_HOST,
  port: process.env.MAILER_PORT,
  auth: {
    user: process.env.MAILER_EMAIL_ID,
    pass: process.env.MAILER_PASSWORD,
  }
});

module.exports = {

  addAuctionBid:async(req,res)=>{
    let paid_amount = 0;
    let final_need_pay = 0;
    try{
      //bid_winner
      // console.log(req.body);
      // return false;
      let {user_id,product_id,bid_id,bid_amount} = req.body;
      let bid_time = new Date();
      
      if(!user_id)
      {
        return res.send({
          status:false,
          message:"Veuillez vous connecter avant de placer une offre",
          url:"",
          paid_amount:paid_amount,
          final_need_pay:final_need_pay
        })
      }
      if(!product_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis",
          url:"",
          paid_amount:paid_amount,
          final_need_pay:final_need_pay
        })
      } 
      if(!bid_amount)
      {
        return res.send({
          status:false,
          message:"Le montant de l'offre est requis",
          url:"",
          paid_amount:paid_amount,
          final_need_pay:final_need_pay
        })
      }

      let today_date = new Date();

      var pro_rec = await Product.findOne({
        _id:product_id,
        auction_start_date:{$lte:today_date}
      });
    //console.log("pro_rec ", pro_rec);
    if(!pro_rec)
    {
      return res.send({
        status:false,
        message:"L'enchère n'est pas encore lancée",
        url:"",
        paid_amount:paid_amount,
        final_need_pay:final_need_pay
      })
    }
    let product_title = pro_rec.title;
      //mongoose.set("debug",true);
      
      //return false;


      
      //let today_date = new Date();
      var pro_rec_auction_over = await Product.findOne({
          _id:product_id,
          auction_end_date:{$gte:today_date}
        });
      //console.log("pro_rec_auction_over ", pro_rec_auction_over);
      if(!pro_rec_auction_over)
      {
        return res.send({
          status:false,
          message:"La vente aux enchères est terminée",
          url:"",
          paid_amount:paid_amount,
          final_need_pay:final_need_pay
        })
      }
      let seting_rec_for_percentage = await SettingModel.findOne({attribute_key: 'auction_amount_percentage'});
      let auction_amount_percentage = 10;
      if(seting_rec_for_percentage)
      {
        auction_amount_percentage = parseFloat(seting_rec_for_percentage.attribute_value);
      }
      var pro_rec_bid_amount = await Product.findOne({
        _id:product_id
      },{auction_price:1});
      //console.log("pro_rec_bid_amount ", pro_rec_bid_amount);

      let last_aution_price = await AuctionbidsModel.findOne({
        product_id:product_id
      }).sort({bid_time:-1});
      // console.log("last_aution_price ", last_aution_price);
      // return false;
      if(last_aution_price)
      {
        let auct_pric = parseFloat(last_aution_price.bid_amount);
        //let minimum_need_to_pay = auct_pric * auction_amount_percentage / 100;
        //console.log("minimum_need_to_pay ", minimum_need_to_pay);
        final_need_pay = parseFloat(last_aution_price.bid_amount) + parseFloat(auction_amount_percentage);
        console.log("last_aution_price bid_amount ", parseFloat(last_aution_price.bid_amount));

        console.log("auction_amount_percentage ", parseFloat(auction_amount_percentage));

        if(final_need_pay > bid_amount )
        {
          return res.send({
            status:false,
            message:"Le montant minimum de l'enchère est de "+final_need_pay+" CFA",
            url:"",
            paid_amount:paid_amount,
            final_need_pay:final_need_pay
          })
        }
      }else{
        if(pro_rec_bid_amount)
        {
          let auct_pric = parseFloat(pro_rec_bid_amount.auction_price);
          //let minimum_need_to_pay = auct_pric * auction_amount_percentage / 100;
          //console.log("minimum_need_to_pay ", minimum_need_to_pay);
          final_need_pay = parseFloat(pro_rec_bid_amount.auction_price) + parseFloat(auction_amount_percentage);

          console.log(" auction_price ", parseFloat(pro_rec_bid_amount.auction_price));

          console.log("auction_amount_percentage ", parseFloat(auction_amount_percentage));

          console.log("final_need_pay ", parseFloat(final_need_pay));
          if(final_need_pay > bid_amount )
          {
            return res.send({
              status:false,
              message:"Le montant minimum de l'enchère est de "+final_need_pay+" CFA",
              url:"",
              paid_amount:paid_amount,
              final_need_pay:final_need_pay
            })
          }
        }
      }

      
      //
      //return false;
      
      
      // console.log("auction_amount_percentage ", auction_amount_percentage);
      // return false;
      let seting_rec = await SettingModel.findOne({attribute_key: 'auction_amount'});
      if(seting_rec)
      {
        paid_amount = parseFloat(seting_rec.attribute_value);
      }
      //AuctioninitiateModel
      const url = customConstant.base_url + "api/product/initiatePayment/"+user_id+"/"+product_id;

      let initiate_check = await AuctioninitiateModel.findOne({user_id:user_id,
        product_id:product_id,is_payment:true});
      if(!initiate_check)  
      {
        return res.send({
          status:false,
          message:"Avant d'enchérir, vous devez verser un certain montant, puis vous pouvez participer à cette vente aux enchères.",
          url:url,
          paid_amount:paid_amount,
          final_need_pay:final_need_pay
        })
      }
      AuctionbidsModel.create({
        user_id:user_id,
        product_id:product_id,
        bid_amount:bid_amount,
        bid_time:bid_time
      }).then(async(result)=>{
        let old_auction_bidding = await AuctionbidsModel.aggregate(
          [
            {
              $match:
              {
                $and:[
                {product_id: product_id},
                {user_id: { $ne:user_id }},
                {bid_time:{$gte:pro_rec.auction_start_date }},
                {bid_time:{ $lte:pro_rec.auction_end_date } }
                ]
              }
            },
            {
              $lookup:{
                from:"users",
                let:{"user_id":{"$toObjectId":"$user_id"}},
                pipeline:[
                  {
                    $match:{
                      $expr:{
                        $eq:[ "$_id","$$user_id"]
                      }
                    }
                  }
                ],
                as:"all_users"
              }
            },
            {
              $lookup:{
                from:"products",
                let:{"pro_id":{"$toObjectId":"$product_id"}},
                pipeline:[
                  {
                    $match:{
                      $expr:{
                        $eq:["$_id","$$pro_id"]
                      }
                    }
                  }
                ],
                as:"product_rec"
              }
            },
            {
              $project:{
                "all_users.email":1,
                "all_users.first_name":1,
                "all_users.last_name":1,
                "product_rec.title":1
              }
            }
          ]);
        //console.log("old_auction_bidding ", JSON.stringify(old_auction_bidding));
        let all_email = old_auction_bidding.map((val)=> val.all_users[0].email);
        console.log("all_email ", all_email);
        // old_auction_bidding.forEach(async(val)=>{
        //   // console.log("val ", val.all_users[0].email);
        //   // console.log("val ", val );
        //   // let email = val.all_users[0].email;
        //   // let fullname = val.all_users[0].first_name+" "+val.all_users[0].last_name;
        // });
        var base_url_server = customConstant.base_url;
        let product_url = "<a href="+customConstant.base_url_web_site+"view-product/"+product_id+">Cliquez ici<a/>";
        var imagUrl = base_url_server+"public/uploads/logo.png";
        var html = "";
        html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
        html += "<div class='content'></div>";
        //html += "<p>	Bienvenue ,</p>";
      // html += "<p>"+fullname+"</p>";
          //html+= `Un nouvel utilisateur a placé une nouvelle offre pour le produit ${product_title} vous pouvez ajouter une nouvelle offre à ${product_url} `;
          html+= `Quelqu’un a surenchérit sur vous. Veuillez surenchérir pour ne pas perdre le produit ${product_url}.`;
          //html+= '<a href="'+product_url+'">'+' pour la vérification de email, cliquez ici '+'</strong>';
        html += "</div>";
        html += "<div class='mail-footer'>";
        html += "<img src='"+imagUrl+"' height='150' width='250'>";
        html += "</div>";
        html += "<i>Rawgadou</i>";
  
        var messageEmail = html;
        const message = {
        from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
        to: all_email,         // recipients
        subject: "Nouvelle offre", // Subject line
        html: messageEmail // Plain text body
        };
        await transport.sendMail(message, function(err, info) {
            if (err) {
                console.log("errr",err);
                //error_have = 1;
                
            } else {
                console.log("no err");
                return res.send({
                  status:true,
                  message:"Le montant de votre enchère a été entièrement placé",
                  url:"",
                  paid_amount:paid_amount,
                  final_need_pay:final_need_pay
                })
            }
        });
        console.log("after email sent hereee");
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message,
          url:"",
          paid_amount:paid_amount,
          final_need_pay:final_need_pay
        })
      });
    }catch(e)
    {
      return res.send({
        status:false,
        message:e.message,
        url:"",
        paid_amount:paid_amount,
        final_need_pay:final_need_pay
      })
    }
  },
  getAuctionPercentage:async(req,res)=>{
    try{
      let seting_rec_for_percentage = await SettingModel.findOne({attribute_key: 'auction_amount_percentage'});
      let auction_amount_percentage = 10;
      if(seting_rec_for_percentage)
      {
        auction_amount_percentage = parseFloat(seting_rec_for_percentage.attribute_value);
      }
      return res.send({
        status:true,
        message:"Success",
        record:auction_amount_percentage
      })
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  initiatePayment:async(req,res)=>{
    try{
      //auction_amount
      //http://localhost:4200/view-product/64e897c44fcbe7c068e5df70
      
      let {user_id,product_id} = req.params;

      //bidCheckout/:user_id/:product_id
      //let url = customConstant.base_url_user_site+"bidCheckout/"+user_id+"/"+product_id;
      
      let payment_date = new Date();
      let seting_rec = await SettingModel.findOne({attribute_key: 'auction_amount'});
      if(seting_rec)
      {
        let amount = seting_rec.attribute_value;
        amount = parseFloat(amount);
        await AuctioninitiateModel.create({
            user_id:user_id,
            product_id:product_id,
            amount:amount,
            payment_date:payment_date
        }).then((result)=>{
          //requestPaymentAuction
          let url = customConstant.base_url+"api/order/requestPaymentAuction/"+result._id;
          res.redirect(303, url);
          // return res.send({
          //   status:true,
          //   message:"Votre paiement a été effectué avec succès."
          // })
        }).catch((e)=>{
          return res.send({
            status:false,
            message:e.message
          })
        })
      }else{
        return res.send({
          status:false,
          message:"Aucun montant disponible pour le paiement"
        })
      }
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  allYourBidProduct:async(req,res)=>{
    try{
      let {user_id,pagno} = req.body;
      if(!user_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est requis"
        })
      }
      
      
      let today_date = new Date();
      const opquery={is_deleted:false};
      let sort=req.body.sort||1;
      sort = parseInt(sort);
      opquery['is_active']=true;
      opquery['is_auction']=true;
      pagno = pagno ? pagno : 0;
      var limit = 10;
      var skip = pagno*limit; 

        //mongoose.set("debug",true);
        await Product.aggregate([
          {
            $match:{
              "is_active":true
            }
          },
          {
            $match:{
              "is_auction":true
            }
          },{
            $match:{
              "auction_end_date":{$gte:today_date}
            }
          },
          {
            $lookup:{
              from:"auctionbids",
              let:{"pro_idd":{"$toString":"$_id"}},
              pipeline:[
                {
                  $match:{
                    $expr:{
                      $eq:["$product_id","$$pro_idd"]
                    }
                  }
                },
                {
                  $match:{
                    $expr:{
                      $eq:["$user_id",user_id]
                    }
                  }
                },
                {
                  $sort:{
                    "bid_time":-1
                  }
                },
                {
                  $limit:1
                }
              ],
              as:"auctionAmount"
            }
         },
         { $match: { auctionAmount: { $ne: [] } } },
         {
           $lookup:{
             from:"auctionbids",
             let:{"pro_idd":{"$toString":"$_id"}},
             pipeline:[
               {
                 $match:{
                   $expr:{
                     $eq:["$product_id","$$pro_idd"]
                   }
                 }
               },
               {
                 $sort:{
                   "bid_time":-1
                 }
               },
               {
                 $limit:1
               }
             ],
             as:"auctionBidLatest"
           }
        }, 
        {
         $project:{
           "auctionAmount.created_at": 0,
           "auctionAmount.product_id": 0,
           "auctionAmount.bid_id": 0,
           "auctionAmount.bid_winner": 0,
           "auctionAmount.bid_time": 0,
           "auctionAmount.__v": 0,

           "auctionBidLatest.created_at": 0,
           "auctionBidLatest.product_id": 0,
           "auctionBidLatest.bid_id": 0,
           "auctionBidLatest.bid_winner": 0,
           "auctionBidLatest.bid_time": 0,
           "auctionBidLatest.__v": 0,
         }
        },
        ]).then((result)=>{
          return res.send({
            status:true,
            message:"Success",
            data:result,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });

    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  allYourBidWon:async(req,res)=>{
    try{
      

      let {user_id,pagno} = req.body;
      if(!user_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est requis"
        })
      }
      
      
      let today_date = new Date();
      const opquery={is_deleted:false};
      let sort=req.body.sort||1;
      sort = parseInt(sort);
      opquery['is_active']=true;
      opquery['is_auction']=true;
      pagno = pagno ? pagno : 0;
      var limit = 10;
      var skip = pagno*limit; 

        //mongoose.set("debug",true);
        await Product.aggregate([
          {
            $match:{
              "is_active":true
            }
          },{
            $match:{
              "auction_end_date":{$lt:today_date}
            }
          },
          {
            $lookup:{
              from:"auctionbids",
              let:{"pro_idd":{"$toString":"$_id"}},
              pipeline:[
                {
                  $match:{
                    $expr:{
                      $eq:["$product_id","$$pro_idd"]
                    }
                  }
                },
                {
                  $match:{
                    $expr:{
                      $eq:["$user_id",user_id]
                    }
                  }
                },
                {
                  $match:{
                    bid_winner:true
                  }
                },
                {
                  $match:{
                    is_payment:false
                  }
                },
                {
                  $sort:{
                    "bid_time":-1
                  }
                },
                {
                  $limit:1
                }
              ],
              as:"auctionAmount"
            }
         },
         { $match: { auctionAmount: { $ne: [] } } },
         {
           $lookup:{
             from:"auctionbids",
             let:{"pro_idd":{"$toString":"$_id"}},
             pipeline:[
               {
                 $match:{
                   $expr:{
                     $eq:["$product_id","$$pro_idd"]
                   }
                 }
               },
               {
                 $match:{
                   bid_winner:true
                 }
               },
               {
                 $sort:{
                   "bid_time":-1
                 }
               },
               {
                 $limit:1
               }
             ],
             as:"auctionBidLatest"
           }
        }, 
        {
         $project:{
           "auctionAmount.created_at": 0,
          //  "auctionAmount.product_id": 0,
          //  "auctionAmount.bid_id": 0,
          //  "auctionAmount.bid_winner": 0,
          //  "auctionAmount.bid_time": 0,
           "auctionAmount.__v": 0,

           "auctionBidLatest.created_at": 0, 
          //  "auctionBidLatest.bid_id": 0,
          //  "auctionBidLatest.bid_winner": 0,
          //  "auctionBidLatest.bid_time": 0,
           "auctionBidLatest.__v": 0,
         }
        },
        ]).then((result)=>{
          return res.send({
            status:true,
            message:"Success",
            data:result,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });

    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  checkAuctionOver:async(req,res)=>{
    try{
      
      let today_date = new Date();

      var pro_rec_auction_over = await Product.aggregate([
        
        {
          $match:{
            "is_auction":true
          }
        },{
          $match:{
            "auction_end_date":{$lt:today_date}
          }
        },
        {
          $lookup:{
            from:"auctionbids",
            let:{"pro_idd":{"$toString":"$_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$product_id","$$pro_idd"]
                  }
                }
              },
              {
                $sort:{
                  "bid_time":-1
                }
              },
              {
                $limit:1
              }
            ],
            as:"auctionAmount"
          }
       },
      // { $match: { auctionAmount: { $ne: [] } } },
        
      {
       $project:{
         "auctionAmount": 1,
         "_id": 1,
       }
      },
      ]);
      for(let x=0; x<pro_rec_auction_over.length; x++)
      {
        console.log("id ",pro_rec_auction_over[x]._id );
        console.log("id ",pro_rec_auction_over[x].auctionAmount );
        if(pro_rec_auction_over[x].auctionAmount)
        {
          if(pro_rec_auction_over[x].auctionAmount.length > 0)
          {
            await AuctionbidsModel.updateOne({_id:pro_rec_auction_over[x].auctionAmount[0]._id},{bid_winner:true})
          }
        }
        await Product.updateOne({_id:pro_rec_auction_over[x]._id},{is_auction:false});
      }
      
      //console.log(today_date);
      // return res.send({
      //   status:true,
      //   message:"Success",
      //   data:pro_rec_auction_over
      // });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  productAllBid:async(req,res)=>{
    try{
      let {product_id} = req.body;
      if(!product_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis"
        })
      } 

      await Product.aggregate([
        {
          $match:{
            "_id":mongoose.Types.ObjectId(product_id)
          }
        },
        {
          $lookup:{
            from:"auctionbids",
            let:{"pro_idd":{"$toString":"$_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$product_id","$$pro_idd"]
                  }
                }
              },
              {
               $addFields:{
                 "usrId":"$user_id"
               }
              },
              {
               $lookup:{
                 from:"users",
                 let:{"usr_Id":{"$toObjectId": "$usrId" } },
                 pipeline:[
                   {
                     $match:{
                       $expr:{
                         $eq:["$_id","$$usr_Id"]
                       }
                     }
                   }
                 ],
                 as:"userRecord"
               }
              },
              {
                $sort:{
                  "bid_time":-1
                }
              }
            ],
            as:"auctionAmount"
          }
       }
      // { $match: { auctionAmount: { $ne: [] } } },
        
      // {
      //  $project:{
      //    "auctionAmount": 1,
      //    "_id": 1,
      //  }
      // },
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Success",
          data:result,
        })
      }).catch((error)=>{
        return res.send({
          status:false,
          message:error.message
        })
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  }
};