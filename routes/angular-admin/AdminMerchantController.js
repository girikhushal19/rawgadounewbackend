const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const UsersModel = require("../../models/user/User");
const ClientsModel = require("../../models/admin/ClientsModel");
const PushNotificationMerchantModel = require("../../models/admin/PushNotificationMerchantModel");
const notificationmodel = require("../../models/Notifications");
const Notifications = require("../../models/Notifications");

const fs = require('fs');
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

module.exports = {

  allMerchant:async function(req,res,next)
	{
		var { first_name,last_name,phone,email,city,status } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['fullName'] = fullName;
	    if(first_name && first_name != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['first_name'] =  {'$regex' : '^'+first_name, '$options' : 'i'};
	    }
	    if(last_name && last_name != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['last_name'] =  {'$regex' : '^'+last_name, '$options' : 'i'};
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] =  {'$regex' : '^'+email, '$options' : 'i'};
	    }
	    if(phone && phone != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['phone'] = phone;
	    }
      
	    if(status && status != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['is_active'] = status;
	    }
      String_qr['is_deleted'] = false;
      String_qr['user_type'] =  'merchant';
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);

	    	
	      UsersModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
	allMerchantForInvoice:async function(req,res,next)
	{
		var { first_name,last_name,phone,email,city,status } = req.body;
		try{
			//mongoose.set('debug', true);
			UsersModel.find( {user_type:'merchant'}, {first_name:1,last_name:1,phone:1}).exec((err, result)=>
			{
				if(err)
				{
					console.log(err);
					var return_response = {"error":true,errorMessage:err };
					res.status(200).send(return_response);
				}else{
					//console.log("result ");
					//console.log(typeof result);
					//console.log("result"+result);
					if(result.length > 0)
					{
						var return_response = {"error":false,errorMessage:"Succès","record":result};
						res.status(200).send(return_response);
					}else{
						var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
						res.status(200).send(return_response);
					}
				}
			});
		}catch(err){
			console.log(err);
			var return_response = {"error":true,errorMessage:err };
			res.status(200).send(return_response);
		}
	},
  allMerchantCount:async function(req,res,next)
	{
		var { first_name,last_name,phone,email,city,status } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['fullName'] = fullName;
	    if(first_name && first_name != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['first_name'] = { $regex: '.*' + first_name + '.*' };
	    }
	    if(last_name && last_name != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['last_name'] = { $regex: '.*' + last_name + '.*' };
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] = { $regex: '.*' + email + '.*' };
	    }
	    if(phone && phone != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['phone'] = phone;
	    }
      
	    if(status && status != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['is_active'] = status;
	    }
      String_qr['is_deleted'] = false;
      String_qr['user_type'] =  'merchant';
	  	//console.log(String_qr);
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    try{
	    	//mongoose.set('debug', true);

	    	
	      UsersModel.countDocuments(String_qr).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  updateMerchantStatusApi: async function(req, res,next)
  {
    try{
      //mongoose.set('debug', true);
      //console.log(req.body.status);
      UsersModel.updateOne({ "_id":req.body.id }, 
      {is_active:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  allAdminClientMerchant:async function(req,res,next)
	{
		
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        var { user_id,phone,email,full_name } = req.body;
        //phone= parseFloat(phone);
        var String_qr = {}; 
        if(full_name && full_name != "")
        {
          //{ "abc": { $regex: '.*' + colName + '.*' } } 
					//{ $regex: '.*' + full_name + '.*' };
          String_qr['full_name'] = {'$regex' : '^'+full_name, '$options' : 'i'};
        }
        if(email && email != "")
        {
          //{ "abc": { $regex: '.*' + colName + '.*' } }
          String_qr['email'] =  {'$regex' : '^'+email, '$options' : 'i'};
        }
        if(phone && phone != "")
        {
          String_qr['mobile_number'] = phone;
        }
        String_qr['user_id'] = user_id;
        //console.log(String_qr);	    	
	      ClientsModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allAdminClientMerchantCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);

	    try{
        var { user_id } = req.body;
        var String_qr = {}; 
        String_qr['user_id'] = user_id;
        ClientsModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  getAdminClientDetail:async function(req,res,next)
	{
    try{
      //mongoose.set('debug', true);
      var { order_id } = req.body;
      var String_qr = {}; 
      String_qr['_id'] = order_id;
      //console.log(String_qr);	    	
      ClientsModel.find( String_qr, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
	},
  getAllActiveMerchantAdminApi:async function(req,res,next)
	{
		//var { first_name,last_name,phone,email,city,status } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/
	    try{
	    	//mongoose.set('debug', true);
	      UsersModel.find( { user_type: 'merchant' , is_deleted: false , is_active: true}, {first_name:1,last_name:1,phone:1}).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
	sendMerchantPushNotificationSubmit:async function(req,res,next)
	{
		try{
			//console.log(req.body);
			var all_user = req.body.all_user; 	
      var title = req.body.title;   
      var description = req.body.description; 	
			
			var user_array = [];
      var counter = 0;
      var counter_second = 0;
			if(all_user.length > 0)
      {
        var total_length_minus = all_user.length - 1;
        all_user.forEach(object => {
          //console.log(object);
          user_array.push(object.item_id);

					// to_id: '641c3f7deaa2ea2bb402d45b',
					// user_type: 'user',
					// title: 'Commande livrée',
					// message: "La commande a été livrée avec succès dans son intégralité lorsque l'identifiant de la commande est - ",
					// status: 'unread',
					// notification_type: 'order_delivered',

					


          
					Notifications.create({
						to_id:object.item_id,
								title:title,
								message:description,
								notification_type:"msg_from_admin"
							},function(err,result){
							if(err)
							{
									var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200)
										.send(return_response);
							}else{
									if(counter == total_length_minus)
									{
										var return_response = {"error":false,success: true,errorMessage:"Réussite complète de l'envoi de la notification"};
										return res.status(200)
										.send(return_response);
									}
							}
							counter++;
						});
						
				});
			}else{
				var return_response = {"error":true,errorMessage:"Veuillez sélectionner l'utilisateur" };
				res.status(200).send(return_response);
			}
		}catch(err)
		{
			console.log(err);
			var return_response = {"error":true,errorMessage:err };
			res.status(200).send(return_response);
		}
	},
	getMerchantNotification:async function(req,res,next)
	{
		try{
			//console.log(req.body);
			var {user_id} = req.body;
			var newMessageCount = 0;
			PushNotificationMerchantModel.count( { user_id: user_id,is_seen:0}).exec((err, resultAllCount)=>
			{
				if(err)
				{
					console.log(err);
				}else{
					newMessageCount = resultAllCount;
					PushNotificationMerchantModel.find( { user_id: user_id,is_seen:0}, {title:1,description:1}).skip(0).limit(3).sort({"created_at":-1}).exec((err, result)=>
					{
						if(err)
						{
							console.log(err);
						}else{
							//console.log("result ");
							//console.log(typeof result);
							//console.log("result"+result);
							if(result.length > 0)
							{
									var return_response = {"error":false,errorMessage:"Succès","newMessageCount":newMessageCount,"record":result};
											res.status(200).send(return_response);
							}else{
								var return_response = {"error":false,errorMessage:"Pas d'enregistrement","newMessageCount":newMessageCount,"record":result};
								res.status(200).send(return_response);
							}

						}
					});

				}
			});

			
		}catch(err)
			{
				console.log(err);
				var return_response = {"error":true,errorMessage:err };
				res.status(200).send(return_response);
		}
	},
	allAdminPushNotification:async function(req,res,next)
	{
		var { user_id } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {to_id:user_id}; 
	    
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);

	    	
	      notificationmodel.find( String_qr, {title:1,message:1,createdd_at:1}).sort({createdd_at:-1}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allAdminPushNotificationCount:async function(req,res,next)
	{
		var { user_id } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {to_id:user_id}; 
	    
	  	//console.log(String_qr);
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    try{
	    	//mongoose.set('debug', true);
	      notificationmodel.countDocuments(String_qr).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
	deletePushNotificationApi:async function(req,res,next)
	{
		var { id } = req.body;
	    try{
	    	//mongoose.set('debug', true);
	      notificationmodel.deleteOne( { "_id" : id }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
						var return_response = {"error":true,errorMessage:err };
        		res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"La notification push a été supprimée avec succès"};
            res.status(200).send(return_response);
          }
        });
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
};