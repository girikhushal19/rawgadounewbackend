const express =require('express');
const ShippingController = require('./ShippingController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/addNewZone',ShippingController.addNewZone)
router.get('/geAlltZones',ShippingController.geAlltZones)

router.get('/deleteZone/:id',ShippingController.deleteZone);
router.get("/getZonesByVendorId/:id", ShippingController.getZonesByVendorId);
router.get("/getZonesByZoneId/:id", ShippingController.getZonesByZoneId);
router.post("/addShippinMethod", ShippingController.addShippinMethod);
router.post("/updateZoneName", ShippingController.updateZoneName);

router.get("/deleteShippingMethod/:id", ShippingController.deleteShippingMethod);
router.get("/getShippingMethodsByVendorId/:id", ShippingController.getShippingMethodsByVendorId);
router.get("/getShippingMethodsByShippingMethodId/:id", ShippingController.getShippingMethodsByShippingMethodId);
router.post("/calculateShippingcost", ShippingController.calculateShippingcost);
router.post("/getshippingMethodsByweight",ShippingController.getshippingMethodsByweight);

router.get("/getmcat",ShippingController.getmcat);
router.get("/getMshippingMethod",ShippingController.getMshippingMethod);
router.post("/calculateShippingcostforM",ShippingController.calculateShippingcostforM);
router.get("/deletemcat/:id",ShippingController.deletemcat);
router.post("/addorUpdate",ShippingController.addorUpdate);
router.post("/addShippinMethodmarketplace",ShippingController.addShippinMethodmarketplace);



module.exports=router








