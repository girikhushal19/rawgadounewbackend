const zonesModel = require("../../models/shipping/Zones");
const shippingModel = require("../../models/shipping/Shipping");
const { Product } = require("../../models/products/Product");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const Subscriptions=require("../../models/subscriptions/Subscription");
const SellerModel=require("../../models/seller/Seller");
const marketCatagoryModel=require("../../models/shipping/marketCatagory");
const mongoose = require("mongoose");
module.exports = {
    addNewZone: async function (req, res) {

        const name = req.body.name;
        const id = req.body.id;
        const cities = req.body.cities;
        const vendor_id = req.body.vendor_id;
        //console.log("req.body",req.body);return false;
        let isitalreadyexists;
        if (id) {
            isitalreadyexists = await zonesModel.findById(id);
        } else {
            isitalreadyexists = null;
        }
        if (isitalreadyexists) {

            if (name) {
                isitalreadyexists.name = name;
            }
            if (cities.length) {
                isitalreadyexists.cities = cities;
            }
            //mongoose.set("debug",true);
            let city_check = await zonesModel.findOne({
                _id:id,
                cities:{$elemMatch:{ city:  req.body.single_city} }
            });
            // console.log("city_check");
            // console.log(city_check);return false;
            if(city_check)
            {
                return res.send({
                    status: false,
                    errorMessage: "Cette ville est déjà enregistrée dans cette zone."
                })
            }else{
                await isitalreadyexists
                .save()
                .then((result) => {
                    console.log("result", result)
                    return res.send({
                        status: true,
                        errorMessage: "La zone a été mise à jour avec succès"
                    })
                })
            }
            
        } else {
            await zonesModel.create({
                name: name,
                cities: cities,
                vendor_id: vendor_id
            }).then((result) => {
                return res.send({
                    status: true,
                    errorMessage: "Zone ajoutée avec succès"
                })
            })
        }
    },
    deleteZone: async function (req, res) {
        const id = req.params.id;
        await zonesModel
            .findByIdAndDelete(id)
            .then((result) => {
                return res.send({
                    status: true,
                    message: "deleted successfully"
                })
            })
    },
    getZonesByVendorId: async function (req, res) {
        const id = req.params.id;
        await zonesModel
            .find({ vendor_id: id })
            .then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "deleted successfully"
                })
            })
    },
    getZonesByZoneId: async function (req, res) {
        const id = req.params.id;
        await zonesModel
            .findById(id)
            .then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "deleted successfully"
                })
            })
    },
    updateZoneName:async function(req,res)
    {
        try{
            console.log(req.body);
            await zonesModel
            .updateOne({ _id: req.body.id },{name:req.body.zone_name})
            .then((result) => {
                return res.send({
                    status: true,
                    message: "Enregistrement complet des succès actualisés"
                })
            })

        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    geAlltZones: async function (req, res) {
        
        await zonesModel
            .find({})
            .then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    errorMessage: "L'enregistrement a été effectué avec succès"
                })
            })
    },
    addShippinMethod: async function (req, res) {
        const {
            method_name,
            zone_list,
            
            min_weight,
            max_weight,
            cost_base,
            cose_per_unit,
            number_of_days,
            
            vendor_id,
            id
        } = req.body;
       

        if (id) {
            await shippingModel.findByIdAndUpdate(id, {
                ...(method_name && { method_name: method_name }),
                ...(vendor_id && { vendor_id: vendor_id }),
                ...(zone_list && { zone_list: zone_list }),
                type:"u",
                ...(min_weight && { min_weight: min_weight }),
                ...(max_weight && { max_weight: max_weight }),
                ...(cost_base && { cost_base: cost_base }),
                ...(cose_per_unit && { cose_per_unit: cose_per_unit }),
                ...(number_of_days && { number_of_days: number_of_days }),
               
            }).then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "updated successfully"
                })
            })
        } else {
            await shippingModel.create({
                method_name: method_name,
                zone_list: zone_list,
                type:"u",
                min_weight: min_weight,
                max_weight: max_weight,
                cost_base: cost_base,
                vendor_id:vendor_id,
                cose_per_unit: cose_per_unit,
                number_of_days: number_of_days,
                
            }).then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "created successfully"
                })
            })
        }
    },
    addShippinMethodmarketplace: async function (req, res) {
        const {
            method_name,
            zone_list,
            catagory_list,
            min_weight,
            max_weight,
            cost_base,
            cose_per_unit,
            number_of_days,
           catagory,
            vendor_id,
            id,
            cost_additional_weight
        } = req.body;
        
        //console.log(req.body);return false;

        if (id) {
            await shippingModel.findByIdAndUpdate(id, {
                min_weight:min_weight,
            });
            await shippingModel.findByIdAndUpdate(id, {
                ...(method_name && { method_name: method_name }),
                ...(vendor_id && { vendor_id: vendor_id }),
                //...(catagory.length && { catagory_list: catagory }),
                ...(zone_list && { zone_list: zone_list }),
                ...(cost_additional_weight && { cost_additional_weight: cost_additional_weight }),
              
                 ...(min_weight && { min_weight: min_weight }),
                ...(max_weight && { max_weight: max_weight }),
                ...(cost_base && { cost_base: cost_base }),
                type:"m",
                ...(cose_per_unit && { cose_per_unit: cose_per_unit }),
                ...(number_of_days && { number_of_days: number_of_days })
            }).then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "L'enregistrement a été mis à jour avec succès"
                })
            })
        } else {
            await shippingModel.create({
                method_name: method_name,
                zone_list: zone_list,
                catagory_list: catagory,
                type:"m",
                min_weight: min_weight,
                max_weight: max_weight,
                cost_base: cost_base,
                vendor_id:vendor_id,
                cost_additional_weight:cost_additional_weight,
                cose_per_unit: cose_per_unit,
                number_of_days: number_of_days
               
            }).then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "Créé avec succès"
                })
            })
        }
    },
    deleteShippingMethod: async function (req, res) {
        const id = req.params.id;
        await shippingModel
            .findByIdAndDelete(id)
            .then((result) => {
                return res.send({
                    status: true,
                    message: "deleted successfully"
                })
            })
    },
    getShippingMethodsByVendorId: async function (req, res) {
        const id = req.params.id;
        await shippingModel
            .aggregate([
                { $match: { vendor_id: id } },
                {$addFields:{vendorIDOBJ:{"$toObjectId":"$vendor_id"}}},
                // {
                //     $lookup: {
                //         from: "zones",
                //         let: { zone_list: "$zone_list" },
                //         pipeline: [
                //             { $match: { "name": { "$in": ["$$zone_list"] } } }
                //         ],
                //         as: "zones"
                //     }
                // },
                {
                    $lookup: {
                        from: "sellers",
                        localField: "vendorIDOBJ",
                        foreignField: "_id",
                        as: "seller"
                    }
                }
            ])
            .then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "deleted successfully"
                })
            })
    },
    getShippingMethodsByShippingMethodId: async function (req, res) {
        const id = req.params.id;
        await shippingModel
            .aggregate([
                { $match: { _id: mongoose.Types.ObjectId(id) } },
            ])
            .then((result) => {
                // console.log("zone_list");
                // console.log(result[0].zone_list);
                //let all_zone = result[0].zone_list
                var arr = [];
                for(let aa of result[0].zone_list)
                {
                    arr.push(aa.zone);
                    //console.log(aa.zone);
                }
                // console.log("arr");
                // console.log(arr);
                return res.send({
                    status: true,
                    data: result,
                    zone_list:arr,
                    message: "successfully"
                })
            })
    },
    getuserprofile:async(req,res)=>{
        await SellerModel.findById(req.params.id).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    calculateShippingcostforproducts: async function (data) {
        //console.log("hereeeeeeeeeeeeee 292");return false;
        var data_1_more = "";
        let returnData;
        const product_id = data.product_id;
        const product_qty = data.product_qty;
        const shipping_city = data.shipping_city;
        const productprice=data.productprice;
        const shipping_idd=data.shipping_idd;
        
        // console.log("data 336",data)
        let productcommission=0;
        let plusperproduct=0;
        let producttax=0
        const product = await Product.findById(product_id);
        const provider= await SellerModel.findById(product?.provider_id);
        const gloabalsettings=await GlobalSettings.findOne({});
        const tax= gloabalsettings?.tax||0;
        producttax=productprice*tax/100*product_qty;
        let sub_id;
        if(provider?.sub_id){
           sub_id= mongoose.Types.ObjectId(provider.sub_id)
        }else{
            // return null
        }
        

        //const subscription=await Subscriptions.findOne({_id:sub_id,end_date:{$gte:new Date()},payment_status:true});

        const subscription = await Subscriptions.findOne({_id:sub_id,payment_status:true}).sort({"created_at":-1});

        //console.log("subscription ",subscription);
        //return false;
        if(product && provider){
            // console.log("product,",product)
            // if(subscription.extracommission=="a"){
            //     productcommission=(provider.admin_commission*productprice)*product_qty
            // }else if(provider.admin_commission_type=="p"){
            //     productcommission=((provider.admin_commission/100)*productprice)*product_qty;
                
            // }
            if(subscription)
            {
                // if(subscription.payment_amount == 0)
                // {

                // }
                plusperproduct = subscription.plusperproduct * product_qty;
                productcommission = ((subscription.extracommission*productprice)/100)*product_qty;
            }else{
                plusperproduct = gloabalsettings?.plusperproduct*product_qty||0;
                productcommission = ((gloabalsettings?.commission*productprice)/100)*product_qty||0;
            }
            const product_catagory = product?.catagory||"undefined";
            const product_weight = product.dispatch_info.weight;
            const vendor_id = product.provider_id;
            const totalweight=product_weight*product_qty;


            let data1={
                //result:result,
                //data_1_more:data_1_more,
                productcommission,
                producttax,
                plusperproduct
            }
            return data1


            //console.log("totalweight",totalweight)
            // const product_dimension = product.dispatch_info.size;
            const product_dimension=[];
            const query = {};
            // if (Object.keys(product_dimension).length) {
            //     if (product_dimension.width && product_dimension.Height && product_dimension.Length) {
            //         let height = product_dimension.Height;
            //         let width = product_dimension.width;
            //         let length = product_dimension.Length;
            //         query["dimension.min_height"] = { $lte: height }
            //         query["dimension.max_height"] = { $gte: height }
            //         query["dimension.min_width"] = { $lte: width }
            //         query["dimension.max_width"] = { $gte: width }
            //         query["dimension.min_length"] = { $lte: length }
            //         query["dimension.max_length"] ={ $gte: length }
            //     } 
            //     // else {
            //     //     return null
            //     // }
            // }
            // if (product_weight) {
            //     query['min_weight'] = { $lte: product_weight };
            //     query['max_weight'] = { $gte: product_weight };
            // }
            // if (product_catagory) {
            //     query["catagory_list"] = { "$elemMatch": {"catagory":product_catagory} }
            // }
            //console.log("data ",data);
            //mongoose.set("debug",true);
            const cityinzone = await zonesModel.aggregate([
                { $match: { "cities": { "$elemMatch": {"city":shipping_city} } } }
            ]);
            //console.log("cityinzone",cityinzone);return false;
            if (cityinzone.length) {
                query["zone_list"] = { "$elemMatch": {"zone":cityinzone[0].name} };
                // query["$and"]=[
                //     {"max_weight":{$gte:totalweight}},
                //     {"min_weight":{$lte:totalweight}}
                // ]

                
                // query["max_weight"]={$gte:totalweight}
                // query["min_weight"]={$lte:totalweight}
                // console.log("query",query)
                // const a=await b();
                // const c =a
                // await b().then((resutl)=>{

                // })
                //mongoose.set("debug",true);

                await shippingModel.aggregate([
                    { $match: query },
                    {
                        $addFields: {
                            "shippingcostforproducts": { $cond:{
                                if:{$eq:[product_qty,1]},then:"$cose_per_unit",else:{
                                $cond:{
                                    if:{$lte:[totalweight,"$max_weight"]},then:"$cost_base",else:false
                                    // else:{
                                    //     $multiply:[{$divide:[totalweight,"$max_weight"]},"$cost_base"]
                                    // }
                                }
                            }
                            } },
                            "bestshippingmethodmindiffbetweenmax":{
                                $subtract:["$max_weight",totalweight]
                            },
                            
                        }
                    },
                    {$sort:{"bestshippingmethodmindiffbetweenmax":1}}
                ]).then((result) => {
                    //console.log("result 441 line",result)
                    let newresult=result?.length?result[0]:{}
                    //console.log("result send for productid==>",product_id,"is",newresult)
                     
                    // if(result[0]?.shippingcostforproducts==false){
                    //     data1=null
                    // }
                    // else{
                    //      data1={
                    //         result,
                    //         productcommission,
                    //         producttax,
                    //         plusperproduct
                    //     }
                    // }
                    data_1_more = result;
                     
                })


                if(!shipping_idd)
                {
                    returnData=  await shippingModel.aggregate([
                        { $match: query },
                        {
                            $addFields: {
                                "shippingcostforproducts": { $cond:{
                                    if:{$eq:[product_qty,1]},then:"$cose_per_unit",else:{
                                    $cond:{
                                        if:{$lte:[totalweight,"$max_weight"]},then:"$cost_base",else:false
                                        // else:{
                                        //     $multiply:[{$divide:[totalweight,"$max_weight"]},"$cost_base"]
                                        // }
                                    }
                                }
                                } },
                                "bestshippingmethodmindiffbetweenmax":{
                                    $subtract:["$max_weight",totalweight]
                                },
                                
                            }
                        },
                        {$sort:{"bestshippingmethodmindiffbetweenmax":1}}
                    ]).then((result) => {
                        //console.log("result 441 line",result)
                        let newresult=result?.length?result[0]:{}
                        //console.log("result send for productid==>",product_id,"is",newresult)
                        let data1;
                        // if(result[0]?.shippingcostforproducts==false){
                        //     data1=null
                        // }
                        // else{
                        //      data1={
                        //         result,
                        //         productcommission,
                        //         producttax,
                        //         plusperproduct
                        //     }
                        // }
                        data1={
                            result:result,
                            data_1_more:data_1_more,
                            productcommission,
                            producttax,
                            plusperproduct
                        }
                        return data1
                    })
                }else{
                    returnData=  await shippingModel.aggregate([
                        { $match: 
                        {
                            _id:mongoose.Types.ObjectId(shipping_idd)
                        } },
                        {
                            $addFields: {
                                "shippingcostforproducts": { $cond:{
                                    if:{$eq:[product_qty,1]},then:"$cose_per_unit",else:{
                                    $cond:{
                                        if:{$lte:[totalweight,"$max_weight"]},then:"$cost_base",else:false
                                        // else:{
                                        //     $multiply:[{$divide:[totalweight,"$max_weight"]},"$cost_base"]
                                        // }
                                    }
                                }
                                } },
                                "bestshippingmethodmindiffbetweenmax":{
                                    $subtract:["$max_weight",totalweight]
                                },
                                
                            }
                        },
                        {$sort:{"bestshippingmethodmindiffbetweenmax":1}}
                    ]).then((result) => {
                        //console.log("result 441 line",result)
                        let newresult=result?.length?result[0]:{}
                        //console.log("result send for productid==>",product_id,"is",newresult)
                        let data1;
                        // if(result[0]?.shippingcostforproducts==false){
                        //     data1=null
                        // }
                        // else{
                        //      data1={
                        //         result,
                        //         productcommission,
                        //         producttax,
                        //         plusperproduct
                        //     }
                        // }
                        data1={
                            result:result,
                            data_1_more:data_1_more,
                            productcommission,
                            producttax,
                            plusperproduct
                        }
                        return data1
                    })
                }
                 
    
            } else {
                return null;
            }
        }else{
            return null
        }
        return returnData
    },
    calculateShippingcost: async function (req,res) {
        const data=req.body;
        const product_id = data.product_id;
        const product_qty = data.product_qty;
        const shipping_city = data.shipping_city;
        let productcommission=0;
        let producttax=0
        const product = await Product.findById(product_id);
        const provider= await SellerModel.findById(product.provider_id);
        
        if(product && provider){
            console.log("product,",product)
            if(provider.admin_commission_type=="a"){
                productcommission=(provider.admin_commission*product.price)*product_qty
            }else if(provider.admin_commission_type=="p"){
                productcommission=((provider.admin_commission/100)*product.price)*product_qty;
                console.log("provider.admin_commission/100",provider.admin_commission/100,"product.price",product.price,"product_qty",product_qty)
            }
            const product_catagory = product?.catagory||"undefined";
            const product_weight = product.dispatch_info.weight;
            const vendor_id = product.provider_id;
            const product_dimension = product.dispatch_info.size;
            const query = {};
            if (Object.keys(product_dimension).length) {
                if (product_dimension.width && product_dimension.Height && product_dimension.Length) {
                    let height = product_dimension.Height;
                    let width = product_dimension.width;
                    let length = product_dimension.Length;
                    query["dimension.min_height"] = { $lte: height }
                    query["dimension.max_height"] = { $gte: height }
                    query["dimension.min_width"] = { $lte: width }
                    query["dimension.max_width"] = { $gte: width }
                    query["dimension.min_length"] = { $lte: length }
                    query["dimension.max_length"] ={ $gte: length }
                } else {
                    return res.send({
                        status: false,
                        message: "all dimension variables required"
                    })
                }
            }
            if (product_weight) {
                query['min_weight'] = { $lte: product_weight };
                query['max_weight'] = { $gte: product_weight };
            }
            if (product_catagory) {
                query["catagory_list"] = { "$elemMatch": {"catagory":product_catagory} }
            }
            const cityinzone = await zonesModel.aggregate([
                { $match: { "cities": { "$elemMatch": {"city":shipping_city} }, vendor_id: vendor_id } }
            ]);
            // console.log("cityinzone",cityinzone)
            if (cityinzone.length) {
                query["zone_list"] = { "$elemMatch": {"zone":cityinzone[0].name} };
                console.log("query",query)
                await shippingModel.aggregate([
                    { $match: query },
                    {
                        $addFields: {
                            "shippingcostforproducts": { $multiply: ["$cose_per_unit", product_qty] }
                        }
                    }
                ]).then((result) => {
                    // console.log("result",result)
                    const data={
                        result,
                        productcommission,
                        producttax
                    }
                    return res.send({
                        status: true,
                        data: data
                    })
                })
    
            } else {
                return res.send({
                    status: true,
                    message: "there is no service, in given address"
                })
            }
        }else{
            return res.send({
                status: true,
                message: "there is no product as such,"
            })
        }
    },
    getshippingMethodsByweight:async function(req,res){
        const weight=req.body.weight;
        console.log("weight",weight)
        const shippingMethods=await shippingModel.aggregate([
            // {
            //     // $match:{
            //     //     $and:[
            //     //         {min_weight:{$lte:weight}},
            //     //         {max_weight:{$gte:weight}}
            //     //     ]
            //     // }
                
            // },
            { "$group": {
                "_id": "$method_name",
                "Minrange":{"$min":"$cose_per_unit"},
                "Maxrange":{"$max":"$cose_per_unit"},
                "name": { "$first": "$method_name" },  //$first accumulator
                "count": { "$sum": 1 },  //$sum accumulator
                items: {$push: '$$ROOT'}
              }},
        ])
        return res.send({
            status:true,
            data:shippingMethods
        })
    },
    addorUpdate:async(req,res)=>{
        const {
            name,
            desc,
            id
        }=req.body
        if(id){
            await marketCatagoryModel.findByIdAndUpdate(id,{
                ...(name && {name:name}),
                ...(desc && {desc:desc})
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"done"
                })
            })
        }else{
           await marketCatagoryModel.create({
                name:name,
                desc:desc
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"done"
                })
            })
        }
    },
    deletemcat:async(req,res)=>{
        const id=req.params.id;
        await marketCatagoryModel.findByIdAndDelete(id).then((result)=>{
            return res.send({
                status:true,
                message:"done"
            })
        })
    },
    getmcat:async(req,res)=>{
        await marketCatagoryModel.find({}).then((result)=>{
            return res.send({
                status:true,
                message:"done",
                data:result
            })
        })
    },
    getMshippingMethod:async(req,res)=>{
        await shippingModel.find({type:"m"}).then((result)=>{
            return res.send({
                status:true,
                message:"done",
                data:result
            })
        })
    },
    calculateShippingcostforM: async function (req,res) {
        const data=req.body;
        const method_name = data.method_name;
        const mcat = data.mcat;
        const weight = data.weight;
        const zone=data.zone;
        console.log('data',data);
        if(method_name == "" || weight == "")
        {
            return res.send({
                status: false,
                message: "Le numéro d'expédition et le poids sont tous deux requis",
                data:[]
            })
        }
        // const shipping=await shippingModel.aggregate([
        //     {$match:{
        //         type:"m",
        //         method_name:method_name,
        //         catagory_list:{"$elemMatch":{$eq:mcat}},
        //         "$and":[
        //            { min_weight:{"$lte":weight}},
        //            { max_weight:{"$gte":weight}}
        //         ]
        //         //  "$and":[
        //         //    { $lte:['min_weight',weight]},
        //         //    { $gte:['max_weight',weight]}
        //         // ]
        //     }}
        // ])

        const shipping=await shippingModel.findOne({_id:method_name});
        let shipping_price = 0;
        //console.log("shipping",shipping)
        if(shipping)
        {
            let qty = 1;
            const product_weight = weight; 
            const product_totalweight=product_weight*qty;
            const product_totalweight_in_gm = product_totalweight * 1000;
            //console.log("product_totalweight ",product_totalweight);
            const max_weight_in_gm = shipping.max_weight * 1000;
            const cost_additional_weight = shipping.cost_additional_weight * 1000;
            const cose_per_unit = shipping.cose_per_unit;
            
            shipping_price = shipping.cost_base;
            if(product_totalweight_in_gm > max_weight_in_gm)
            {
                let remaining_weight = product_totalweight_in_gm - max_weight_in_gm;
                let one_unit_in_gm =remaining_weight / cost_additional_weight;
                //console.log("one_unit_in_gm ",one_unit_in_gm);
                let total_additional_price = one_unit_in_gm * cose_per_unit;
                //console.log("=======================> 1945 ");
                //console.log("total_additional_price ",total_additional_price);
                shipping_price = shipping_price + total_additional_price;
            }
        }
        //return false;
        if(shipping){
            return res.send({
                status: true,
                message: "success",
                data:shipping_price
            })
        }else{
            return res.send({
                status: false,
                message: "Pas d'expédition disponible"
            })
        }
    },
}