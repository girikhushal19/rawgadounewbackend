const subscriptions = require("../../models/subscriptions/Subscription");
const subpackagemodel = require("../../models/subscriptions/SubscriptionPackages");
const sellerModel=require("../../models/seller/Seller");
const base_url = process.env.BASE_URL;
const { addmonthstodate } = require("../../modules/dates");

const stringGen=require("../../modules/randomString");
module.exports = {
  createSubscription: async (req, res) => {
    try {
      const combo_id=stringGen(20);
      //console.log(req.body);return false;
      const {
        subscriber_name,
        subscriber_email,
        
        name_of_package,
        provider_id,
        duration,
        package_id,
      } = req.body;
      if(!subscriber_name || subscriber_name == null || subscriber_name == "")
      {
        return res.send({
          status: false,
          message: "Le nom de l'abonné est requis"
        })
      }
      // if(!name_of_package || name_of_package == null || name_of_package == "")
      // {
      //   return res.send({
      //     status: false,
      //     message: "Le nom du paquet est requis"
      //   })
      // }

      if(!package_id || !subscriber_email)
      {
        return res.send({
          status: false,
          message: "l'identifiant du paquet et l'adresse électronique de l'abonné sont requis"
        })
      }
      const subpackage = await subpackagemodel.findById(package_id);
      //console.log(subpackage);return false;
      if (!subpackage) {
        return res.send({
          status: false,
          message: "Le paquet avec cet identifiant n'existe pas"
        })
      }
      await subscriptions.find({
        subscriber_email: subscriber_email,
        payment_status: false
      }).then((result) => {
        console.log("result", result);
        if (result.length > 0) {
          // console.log("result", result);
          if (result[0].payment_status == true) {
            return res.send({
              status: false,
              message: "",
              errmessage: "L'abonnement existe déjà",
              data: result,

            });
          } else {
            const start_date = new Date().toISOString();
            const end_date = addmonthstodate(start_date, duration);
            const data = {
              start_date: start_date,
              end_date: end_date,
              plusperproduct: subpackage.plusperproduct,
              extracommission: subpackage.extracommission,
              Numberofitems:subpackage.Numberofitems,
              duration:subpackage.duration,
              is_limited:subpackage.is_limited,
              order_id:combo_id
            }
            subscriptions.findByIdAndUpdate(result[0]._id, data, { new: true }).then((service) => {

              /* url:
                  base_url +
                  "/api/common/getPaymentForSub/" +
                  service._id +
                  "/" +
                  subpackage.price, */
              return res.send({
                status: true,
                message: "Abonnement créé avec succès",
                data: result,
                errmessage: "",
                url:
                  base_url +
                  "/api/order/getPaymentForSub/" +
                  combo_id +
                  "/" +
                  subpackage.price,
              });
            });
          }
        } else {
          let payment_status = false;
          if(subpackage.price == 0)
          {
            payment_status = true;
          }
          //console.log("hereee 73",subpackage);
          const start_date = new Date().toISOString();
          const end_date = addmonthstodate(start_date, duration);

          const subscription = new subscriptions();
          subscription.subscriber_name = subscriber_name;
          subscription.subscriber_email = subscriber_email;
          // subscription.type_of_subscription = type_of_subscription;
          subscription.name_of_package = name_of_package;
          subscription.plusperproduct=subpackage.plusperproduct,
          subscription.extracommission=subpackage.extracommission
          subscription.status = true;
          subscription.payment_status = payment_status;
          subscription.start_date = start_date;
          subscription.end_date = end_date;
          subscription.payment_amount=subpackage.price;
          subscription.Numberofitems=subpackage.Numberofitems;
          subscription.duration=subpackage.duration;
          subscription.is_limited=subpackage.is_limited;
          subscription.order_id=combo_id;
          subscription.package_id = package_id;
          subscription.provider_id = req.body.provider_id;
          subscription.save().then((service) => {
           if(subpackage.price > 0)
           {
            console.log("herrrrrrrrrrrrrrrr");
            //console.log("hereee 93",subpackage);
            /*url:
                base_url +
                "/api/common/getPaymentForSub/" +
                service._id +
                "/" +
                subpackage.price,*/
            //subscriptions.find({ _id:service._id })
            res.send({
              status: true,
              message: "Abonnement créé avec succès",
              data: result,
              errmessage: "",
              url:
                base_url +
                "/api/order/getPaymentForSub/" +
                combo_id +
                "/" +
                subpackage.price,
            });
           }else{

            sellerModel.updateOne({_id:provider_id},{sub_id:service._id}).then((ress)=>{
              res.send({
                status: true,
                message: "Abonnement créé avec succès",
                data: result,
                errmessage: "",
               
              });
            }).catch((err)=>{
              res.send({
                status: false,
                message: err.message,
                data: result,
                errmessage: err.message,
               
              });
            })
             
            
           }
          });
        }
      });
    } catch (err) {
      console.log(err)
      res.send({
        status: false,
        message: "",
        errmessage: err.message,
        data: null,
      });
    }
  },
  updateSubscription: async (req, res) => {
    const {
      subscriber_name,
      subscriber_email,
      type_of_subscription,
      name_of_package,
      status,
      start_date,
      end_date,
      subscription_id,
    } = req.body;
    const newenddate = new Date(end_date);
    const newstartdate = new Date(start_date);

    const data = {
      ...(subscriber_name && { subscriber_name }),
      ...(subscriber_email && { subscriber_email }),
      ...(type_of_subscription && { type_of_subscription }),
      ...(name_of_package && { name_of_package }),
      ...(status && { status }),
      ...(start_date && { start_date: newstartdate }),
      ...(end_date && { end_date: newenddate }),
    };
    console.log("data", data)
    const sub = await subscriptions.findById(subscription_id);
    sub.end_date = newenddate;
    sub.start_date = newstartdate;
    sub.payment_status = true;

    sub.save().then((result) => {
      res.send({
        status: true,
        message: "L'abonnement a été mis à jour avec succès",
        data: result,
        errmessage: "",
      });
    })
      .catch((err) => {
        res.send({
          status: false,
          message: "Abonnement non mis à jour",
          data: null,
          errmessage: err.message,
        });
      });
  },
  getAllSubscriptions: (req, res) => {
    subscriptions.find({}).then((result) => {
      res.send({
        status: "success",
        message: "Subscriptions fetched successfully",
        data: result,
        errmessage: "",
      });
    });
  },
  getSubscriptionById: (req, res) => {
    const subscription_id = req.params.subscription_id;
    subscriptions
      .findById(subscription_id)
      .then((result) => {
        res.send({
          status: "success",
          message: "Subscription fetched successfully",
          data: result,
          errmessage: "",
        });
      })
      .catch((err) => {
        res.send({
          status: "error",
          message: "Subscription not fetched",
          data: err,
          errmessage: err.message,
        });
      });
  },
  deleteSubscription: (req, res) => {
    const subscription_id = req.params.subscription_id;
    subscriptions
      .findByIdAndDelete(subscription_id)
      .then((result) => {
        res.send({
          status: true,
          message: "Abonnement supprimé avec succès",
          data: result,
          errmessage: "",
        });
      })
      .catch((err) => {
        res.send({
          status: false,
          message: "",
          data: null,
          errmessage: err.message,
        });
      });
  },
  getSubscriptionbylawyerID: async (req, res) => {
    const lawyer_id = req.params.lawyer_id;
    await subscriptions
      .find({ lawyer_id: lawyer_id })
      .then((result) => {
        res.send({
          status: "success",
          message: "Subscriptions fetched successfully",
          data: result,
          errmessage: "",
        });
      })
      .catch((err) => {
        res.send({
          status: "error",
          message: "",
          data: err,
          errmessage: err.message,
        });
      });
  },
  getSubscriptionbylawyerIDred: async (req, res) => {
    const lawyer_id = req.params.lawyer_id;
    const subscription = await subscriptions.aggregate([
      { $match: { lawyer_id: lawyer_id } },

      { $project: { type_of_subscription: 1 } }
    ]).then((result) => {
      res.send({
        status: "success",
        message: "Subscriptions fetched successfully",
        data: result,
        errmessage: "",
      });
    })
      .catch((err) => {
        res.send({
          status: "error",
          message: "",
          data: err,
          errmessage: err.message,
        });
      });
  },
  getSubscriptionProviderId:async(req,res)=>{
    try{
      let provider_id = req.body.provider_id;
      if(!provider_id)
      {
        return res.send({
          status: false,
          message: "L'identifiant du prestataire est requis",
          data: [],
          errmessage: "L'identifiant du prestataire est requis",
        });
      }
      await subscriptions.find({provider_id:provider_id,payment_status:true}).then((result)=>{
        return res.send({
          status: true,
          message: "Succès",
          data: result,
          errmessage: "Succès",
        });
      }).catch((error)=>{
        return res.send({
          status: false,
          message: error.messag,
          data: [],
          errmessage: error.message,
        });
      });
    }catch(err){
      return res.send({
        status: false,
        message: err.messag,
        data: [],
        errmessage: err.message,
      });
    }
  }
}

