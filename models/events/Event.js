const mongoose = require("mongoose");
const ticketSchema=new mongoose.Schema({
  type:{type:String},
  price:{type:Number}
});
const datetimeSchema=new mongoose.Schema({
  date:{type:Date,default:new Date()},
  time:{type:String,default:""}
});
const AddressSchema=new mongoose.Schema({
  latlong:{type:Array,default:[]},
  address:{type:String,default:""}

})
const EventSchema = new mongoose.Schema({
 
    title:{type:String},
    performers:{type:Array,default:[]},
    org_id:{type:String,default:""},
    org_name:{type:String,default:""},
    description:{type:String,default:""},
    paidornot:{type:Boolean,default:false},
    status:{type:Boolean,default:false},
    type:{type:String,default:""},
    catagory:{type:String,default:""},
    media:{type:Array,default:[]},
    tags:{type:Array,default:[]},
    is_deleted:{type:Boolean,default:false},
    tickets:{type:[ticketSchema],default:[]},
    modeofevent:{type:String},
    datetime:{type:[datetimeSchema],default:[]},
    address:{type:[AddressSchema],default:[]},
    location_cor:{ 
      type: {
          type: String,
          default: "Point"
      },
      coordinates: {
          type: [Number],
          default:[0.0000,0.0000]
      }
    },
    price:{type:Number},
    totoal_ticket:{type:Number,default:0},
});

EventSchema.statics.makeActive=async function(id){
    let event=await this.findOne({_id:id})
    event.is_active=true;
    event.save().then(()=>true)
  }
  EventSchema.statics.makeInActive=async function(id){
    let event=await this.findOne({_id:id})
    event.is_active=false;
    event.save().then(()=>true)
      
    }
  EventSchema.statics.softdelete=async function(id){
    let event=await this.findOne({_id:id});
    event.is_deleted=true
    event.save().then(()=>true)
  }
EventSchema.index({"location_cor": '2dsphere'});

module.exports = mongoose.model("Event", EventSchema);

// {
//         date:{type:Date},
//         time:{type:String}
//     }