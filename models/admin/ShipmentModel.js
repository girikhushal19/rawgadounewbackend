const mongoose = require("mongoose");
const Schema = require("mongoose");

const ShipmentsSchema = new mongoose.Schema({
  order_id: { type: String, default: null },
  invoice_id: [{ type: Schema.Types.ObjectId,ref:"invoices" , default: null}],
  tracking_number: { type: String, default: null },
  client_id: [{ type: Schema.Types.ObjectId,ref:"clients" , default: null}],
  user_id: [{ type: Schema.Types.ObjectId,ref:"User" }],
  full_name: { type: String, default: null },
  email: { type: String, default: null },
  contact_person_name: { type: String, default: null },
  mobile_number: { type: Number, default: null },
  contact_person: { type: String, default: null },
  streat_po_box: { type: String, default: null },
  office_code: { type: String, default: null },
  building_number: { type: String, default: null },
  lift: { type: String, default: null },
  floor: { type: String, default: null },
  intercom: { type: String, default: null },
  locality: { type: String, default: null },
  sender_contact_number: { type: Number, default: null },
  sender_address: { type: String, default: null },
  sender_location: {
   type: { type: String },
   coordinates: []
  },
  pickup_address: { type: String, default: null },
  pickup_location: {
   type: { type: String },
   coordinates: []
  },
  receiver_name: { type: String, default: null },
  receiver_contact: { type: String, default: null },
  receiver_phone: { type: String, default: null },
  receiver_email: { type: String, default: null },
  receiver_locality: { type: String, default: null },
  receiver_streat_po_box: { type: String, default: null },
  receiver_office_code: { type: String, default: null },
  receiver_building_number: { type: String, default: null },
  receiver_entrance_code: { type: String, default: null },
  receiver_floor: { type: String, default: null },
  receiver_apartment: { type: String, default: null },
  receiver_intercom: { type: String, default: null },
  receiver_additional_information: { type: String, default: null },
  receiver_address: { type: String, default: null },
  receiver_location: {
   type: { type: String },
   coordinates: []
  },
  shipment_type: { type: String, default: null },
  shipping_type: { type: String, default: null },
  total_weight: { type: Number, default: null },
  quantity: { type: Number, default: null },
  width: { type: Number, default: null },
  height: { type: Number, default: null },
  length: { type: Number, default: null },
  shipment_note: { type: String, default: null },
  extra_service: { type: Boolean, default: false },
  total_shipping_price: { type: Number, default: null },
  shipping_price: { type: Number, default: null },
  return_price: { type: Number, default: null },
  status: { type: Number, default: 1 }, // 1 in progress 2 shipped 3 delivered 4 cancled
  is_invoiced:{ type: Number, default: 0 }, //0 Not created , 1 created
  shipment_date: { type: Date, default:null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

ShipmentsSchema.index({sender_location: '2dsphere'});
ShipmentsSchema.index({pickup_location: '2dsphere'});
ShipmentsSchema.index({receiver_location: '2dsphere'});
module.exports = mongoose.model("shipments", ShipmentsSchema);