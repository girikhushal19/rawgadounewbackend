const mongoose=require("mongoose");
const PaymentkeysSchema=new mongoose.Schema({
    app_key:{type:String,default:""},
    secrete_key:{type:String,default:""},
    public_key:{type:String,default:""},
    mode:{type:String,default:""},
    created_at:{type:Date,default:Date.now },
    updated_at:{type:Date,default:Date.now }
});

module.exports=mongoose.model("paymentkeys",PaymentkeysSchema);