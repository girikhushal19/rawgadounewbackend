const mongoose=require("mongoose");
const GlobalSettingsSchema=mongoose.Schema({
   
    tax:{type:Number,default:0},
    commission:{type:Number,default:0},
    plusperproduct:{type:Number,default:0},
    returnprice:{type:Number,default:0},
    eventcom:{type:Number,default:0},
   
});
module.exports=mongoose.model("GlobalSettings",GlobalSettingsSchema);