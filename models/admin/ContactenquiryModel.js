const mongoose=require("mongoose");
const ContactenquirySchema=mongoose.Schema({
    
    name:{type:String,default:null},
    email:{type:String,default:null},
    mobile:{type:Number,default:null},
    message:{type:String,default:null},
});
module.exports=mongoose.model("contactenquiry",ContactenquirySchema);